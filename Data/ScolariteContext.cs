﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NavigationV3.Models;

    public class ScolariteContext : DbContext
    {
        public ScolariteContext (DbContextOptions<ScolariteContext> options)
            : base(options)
        {
        }

        public DbSet<NavigationV3.Models.Groupe>? Groupe { get; set; }

        public DbSet<NavigationV3.Models.Etudiant>? Etudiant { get; set; }

        public DbSet<NavigationV3.Models.Matiere>? Matiere { get; set; }

        public DbSet<NavigationV3.Models.Inscription>? Inscription { get; set; }
    }
