﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace NavigationV3.Migrations
{
    [DbContext(typeof(ScolariteContext))]
    [Migration("20220624151551_mg1")]
    partial class mg1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("NavigationV3.Models.Etudiant", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<DateTime>("DateN")
                        .HasColumnType("datetime2");

                    b.Property<int>("GroupeId")
                        .HasColumnType("int");

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.Property<string>("Prenom")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.HasKey("Id");

                    b.HasIndex("GroupeId");

                    b.ToTable("Etudiant");
                });

            modelBuilder.Entity("NavigationV3.Models.Groupe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("LibGroupe")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.HasKey("Id");

                    b.ToTable("Groupe");
                });

            modelBuilder.Entity("NavigationV3.Models.Inscription", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("EtudiantId")
                        .HasColumnType("int");

                    b.Property<int>("MatiereId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EtudiantId");

                    b.HasIndex("MatiereId");

                    b.ToTable("Inscription");
                });

            modelBuilder.Entity("NavigationV3.Models.Matiere", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("LibMatiere")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("nvarchar(30)");

                    b.HasKey("Id");

                    b.ToTable("Matiere");
                });

            modelBuilder.Entity("NavigationV3.Models.Etudiant", b =>
                {
                    b.HasOne("NavigationV3.Models.Groupe", "Groupe")
                        .WithMany("Etudiants")
                        .HasForeignKey("GroupeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Groupe");
                });

            modelBuilder.Entity("NavigationV3.Models.Inscription", b =>
                {
                    b.HasOne("NavigationV3.Models.Etudiant", "Etudiant")
                        .WithMany("Inscriptions")
                        .HasForeignKey("EtudiantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("NavigationV3.Models.Matiere", "Matiere")
                        .WithMany("Inscriptions")
                        .HasForeignKey("MatiereId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Etudiant");

                    b.Navigation("Matiere");
                });

            modelBuilder.Entity("NavigationV3.Models.Etudiant", b =>
                {
                    b.Navigation("Inscriptions");
                });

            modelBuilder.Entity("NavigationV3.Models.Groupe", b =>
                {
                    b.Navigation("Etudiants");
                });

            modelBuilder.Entity("NavigationV3.Models.Matiere", b =>
                {
                    b.Navigation("Inscriptions");
                });
#pragma warning restore 612, 618
        }
    }
}
